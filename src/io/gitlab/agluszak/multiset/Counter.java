package io.gitlab.agluszak.multiset;

public class Counter {
    private int count;

    public Counter() {
        this.count = 0;
    }

    public Counter(int initialCount) {
        this.count = initialCount;
    }

    public int getCount() {
        return this.count;
    }

    public void add(int number) {
        this.count += number;
    }

    public void remove(int number) {
        this.count -= number;
    }

    public void increase() {
        add(1);
    }

    public void decrease() {
        remove(1);
    }
}
