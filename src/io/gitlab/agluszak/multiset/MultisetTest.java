package io.gitlab.agluszak.multiset;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MultisetTest {
    private static String TEST_STRING = "asd";
    private static String OTHER_STRING = "qwerty";

    private Multiset<String> createMultisetWithTwoTestStrings() {
        Multiset<String> multiset = new Multiset<>();
        multiset.add(TEST_STRING);
        multiset.add(TEST_STRING);
        return multiset;
    }

    @Test
    public void addOne() {
        Multiset<String> multiset = new Multiset<>();
        assertFalse(multiset.contains(TEST_STRING));
        multiset.add(TEST_STRING);
        assertTrue(multiset.contains(TEST_STRING));
    }

    @Test
    public void addMany() {
        Multiset<String> multiset = new Multiset<>();
        multiset.add(TEST_STRING, 999);
        multiset.add(OTHER_STRING, 3333);
        assertEquals(999 + 3333, multiset.size());
    }

    @Test
    public void removeMany() {
        Multiset<String> multiset = new Multiset<>();
        multiset.add(TEST_STRING, 999);
        multiset.add(OTHER_STRING, 3333);
        multiset.remove(TEST_STRING, 300);
        assertEquals(999 - 300, multiset.getCount(TEST_STRING));
        assertEquals(3333 + 999 - 300, multiset.size());
    }

    @Test
    public void clear() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        multiset.clear();
        assertTrue(multiset.isEmpty());
    }

    @Test
    public void removeDeletesFromKeyset() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        multiset.remove(TEST_STRING, 2);
        assertTrue(multiset.elementSet().isEmpty());
    }

    @Test
    public void addAllWithMultiset() {
        Multiset<String> multiset = new Multiset<>();
        multiset.add(TEST_STRING);
        multiset.add(TEST_STRING);
        assertEquals(2, multiset.size());
        assertTrue(multiset.contains(TEST_STRING));
        multiset.remove(TEST_STRING);
        assertTrue(multiset.contains(TEST_STRING));
    }

    @Test
    public void containsOne() {
        Multiset<String> multiset = new Multiset<>();
        assertFalse(multiset.contains(TEST_STRING));
        multiset.add(TEST_STRING);
        assertTrue(multiset.contains(TEST_STRING));
    }

    @Test
    public void containsAllWithNormalCollection() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        List<String> list = new ArrayList<>();
        list.add(TEST_STRING);
        list.add(TEST_STRING);
        list.add(TEST_STRING);
        assertTrue(multiset.containsAll(list));
    }

    @Test
    public void containsAllWithEmptyMultiset() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        Multiset<String> emptyMultiset = new Multiset<>();
        assertTrue(multiset.containsAll(emptyMultiset));
    }

    @Test
    public void intersectionWithEmptyMultiset() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        Multiset<String> emptyMultiset = new Multiset<>();
        boolean modified = multiset.retainAll(emptyMultiset);
        assertTrue(modified);
        assertTrue(multiset.isEmpty());
    }

    @Test
    public void sumWithEmptyMultiset() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        Multiset<String> emptyMultiset = new Multiset<>();
        boolean modified = multiset.addAll(emptyMultiset);
        assertFalse(modified);
        assertEquals(2, multiset.getCount(TEST_STRING));
    }

    @Test
    public void differenceWithEmptyMultiset() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        Multiset<String> emptyMultiset = new Multiset<>();
        boolean modified = multiset.removeAll(emptyMultiset);
        assertFalse(modified);
        assertEquals(2, multiset.getCount(TEST_STRING));
    }

    @Test
    public void containsAllWithMultiset() {
        Multiset<String> firstMultiset = createMultisetWithTwoTestStrings();
        Multiset<String> secondMultiset = createMultisetWithTwoTestStrings();
        secondMultiset.add(TEST_STRING);
        assertFalse(firstMultiset.containsAll(secondMultiset));
    }

    @Test
    public void newMultisetIsEmpty() {
        Multiset<String> multiset = new Multiset<>();
        assertTrue(multiset.isEmpty());
    }

    @Test
    public void removeOne() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        multiset.remove(TEST_STRING);
        assertTrue(multiset.contains(TEST_STRING));
        multiset.remove(TEST_STRING);
        assertFalse(multiset.contains(TEST_STRING));
    }

    @Test
    public void multisetSubtraction() {
        Multiset<String> firstMultiset = createMultisetWithTwoTestStrings();
        Multiset<String> secondMultiset = new Multiset<>();
        secondMultiset.add(TEST_STRING);
        firstMultiset.removeAll(secondMultiset);
        assertTrue(firstMultiset.contains(TEST_STRING));
    }

    @Test
    public void removeAllWithNormalCollection() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        List<String> list = new ArrayList<>();
        list.add(TEST_STRING);
        multiset.removeAll(list);
        assertTrue(multiset.isEmpty());
    }

    @Test
    public void reatinAllWithMultiset() {
        Multiset<String> firstMultiset = createMultisetWithTwoTestStrings();
        firstMultiset.add(OTHER_STRING);
        Multiset<String> secondMultiset = new Multiset<>();
        secondMultiset.add(TEST_STRING);
        firstMultiset.retainAll(secondMultiset);
        assertEquals(1, firstMultiset.getCount(TEST_STRING));
        assertEquals(0, firstMultiset.getCount(OTHER_STRING));
    }

    @Test
    public void retainAllWithNormalCollection() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        multiset.add(OTHER_STRING);
        List<String> list = new ArrayList<>();
        list.add(TEST_STRING);
        multiset.retainAll(list);
        assertEquals(2, multiset.getCount(TEST_STRING));
        assertEquals(0, multiset.getCount(OTHER_STRING));
    }

    @Test
    public void testIterator() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        multiset.add(OTHER_STRING);
        int testStringCount = 0;
        int otherStringCount = 0;
        for (String s : multiset) {
            if (s.equals(TEST_STRING)) {
                testStringCount++;
            } else if (s.equals(OTHER_STRING)) {
                otherStringCount++;
            }
        }
        assertEquals(2, testStringCount);
        assertEquals(1, otherStringCount);
    }

    @Test
    public void toArray() {
        Multiset<String> multiset = createMultisetWithTwoTestStrings();
        multiset.add(OTHER_STRING);
        assertEquals(multiset.size(), multiset.toArray(new String[0]).length);
    }
}
