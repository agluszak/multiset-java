package io.gitlab.agluszak.multiset;

import java.util.*;

public class Multiset<E> extends AbstractCollection<E> {
    private Map<E, Counter> hashmap;
    private int size;

    public Multiset() {
        this.hashmap = new HashMap<>();
        this.size = 0;
    }

    @Override
    public boolean add(E e) {
        add(e, 1);
        return true;
    }

    /**
     * Dodaje do multizbioru @p count kopii elementu @e
     *
     * @param e     Element, którego kopie dodajemy
     * @param count Ilość sztuk elementu
     * @return Ilość kopii elementu po wykonaniu operacji
     */
    public int add(E e, int count) {
        Objects.requireNonNull(e);
        if (count < 0) {
            throw new IllegalArgumentException();
        }
        if (hashmap.containsKey(e)) {
            Counter counter = hashmap.get(e);
            counter.add(count);
            size += count;
            return counter.getCount();
        } else {
            hashmap.put(e, new Counter(count));
            size += count;
            return count;
        }
    }

    public Set<E> elementSet() {
        return Collections.unmodifiableSet(hashmap.keySet());
    }

    /**
     * Suma teoriomnogościowa dwóch multizbiorów
     *
     * @param otherMultiset
     * @return Czy kolekcja została zmieniona w wyniku operacji
     */
    public boolean addAll(Multiset<E> otherMultiset) {
        boolean modified = false;
        for (E e : otherMultiset.elementSet()) {
            int countBefore = getCount(e);
            int countAfter = this.add(e, otherMultiset.getCount(e));
            modified = modified || (countAfter != countBefore);
        }
        return modified;
    }

    /**
     * Teoriomnogościowa różnica dwóch zbiorów
     *
     * @param otherMultiset
     * @return Czy kolekcja została zmieniona w wyniku działania operacji
     */
    public boolean removeAll(Multiset<E> otherMultiset) {
        boolean modified = false;
        for (E e : otherMultiset.elementSet()) {
            int countBefore = getCount(e);
            int countAfter = this.remove(e, otherMultiset.getCount(e));
            modified = modified || (countAfter != countBefore);
        }
        return modified;
    }

    /**
     * Teoriomnogościowy iloczyn dwóch zbiorów
     *
     * @param otherMultiset
     * @return Czy kolekcja została zmieniona w wyniku działania operacji
     */
    public boolean retainAll(Multiset<E> otherMultiset) {
        boolean modified = false;
        for (E e : this.elementSet()) {
            int countBefore = getCount(e);
            int countInOther = otherMultiset.getCount(e);
            int copiesToRemove = Math.max(countBefore - countInOther, 0);
            int countAfter = this.remove(e, copiesToRemove);
            modified = modified || (countAfter != countBefore);
        }
        return modified;
    }

    /**
     * Sprawdza czy @p otherMultiset zawiera się w całości w tym zbiorze
     *
     * @param otherMultiset
     * @return Czy się zawiera
     */
    public boolean containsAll(Multiset<E> otherMultiset) {
        for (E e : otherMultiset.elementSet()) {
            if (getCount(e) != otherMultiset.getCount(e)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Zwraca ilość wystąpień elementu @p o w multizbiorze
     *
     * @param o
     * @return Ilość wystąpień
     */
    public int getCount(Object o) {
        Objects.requireNonNull(o);
        if (hashmap.containsKey(o)) {
            return hashmap.get(o).getCount();
        } else {
            return 0;
        }
    }

    @Override
    public boolean contains(Object o) {
        Objects.requireNonNull(o);
        return hashmap.containsKey(o);
    }

    @Override
    public boolean remove(Object o) {
        Objects.requireNonNull(o);
        if (hashmap.containsKey(o)) {
            Counter counter = hashmap.get(o);
            int currentCount = counter.getCount();
            if (currentCount == 1) {
                hashmap.remove(o);
                size--;
                return true;
            } else {
                counter.decrease();
                size--;
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Usuwa z multizbioru @p count kopii elementu @p o
     *
     * @param o     Element, którego kopie usuwamy
     * @param count Ilość kopii
     * @return Ilość kopii elementu po wykonaniu operacji
     */
    public int remove(Object o, int count) {
        Objects.requireNonNull(o);
        if (hashmap.containsKey(o)) {
            Counter counter = hashmap.get(o);
            int currentCount = counter.getCount();
            if (currentCount - count < 1) {
                hashmap.remove(o);
                size -= currentCount;
                return 0;
            } else {
                counter.remove(count);
                size -= count;
                return currentCount - count;
            }
        } else {
            return 0;
        }
    }

    @Override
    public void clear() {
        hashmap.clear();
        size = 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new MultisetIterator();
    }

    @Override
    public int size() {
        return size;
    }

    private class MultisetIterator implements Iterator<E> {
        Iterator<E> hashmapIterator;
        int currentInstanceIndex;
        int currentInstaceCount;
        E currentElement;
        E lastReturnedElement;
        boolean alreadyDeleted;

        MultisetIterator() {
            hashmapIterator = hashmap.keySet().iterator();
            currentInstanceIndex = 0;
            alreadyDeleted = false;
        }

        @Override
        public boolean hasNext() {
            if (currentElement == null) {
                if (hashmapIterator.hasNext()) {
                    currentElement = hashmapIterator.next();
                    currentInstaceCount = hashmap.get(currentElement).getCount();
                    return true;
                } else {
                    return false;
                }
            } else {
                if (currentInstanceIndex < currentInstaceCount) {
                    return true;
                } else {
                    if (hashmapIterator.hasNext()) {
                        currentElement = hashmapIterator.next();
                        currentInstaceCount = hashmap.get(currentElement).getCount();
                        currentInstanceIndex = 0;
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

        @Override
        public E next() {
            if (!hasNext())
                throw new NoSuchElementException();
            currentInstanceIndex++;
            lastReturnedElement = currentElement;
            alreadyDeleted = false;
            return currentElement;
        }

        @Override
        public void remove() {
            if (lastReturnedElement == null || alreadyDeleted) {
                throw new IllegalStateException();
            } else {
                Multiset.this.remove(lastReturnedElement);
                alreadyDeleted = true;
            }
        }
    }
}
